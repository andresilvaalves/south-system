# South-System
###### André Silva Alves [andresilvaalves](https://gitlab.com/andresilvaalves "andresilvaalves")
:fa-linux:

```bash
$ mkdir ~/data/ && mkdir ~/data/in && mkdir ~/data/out
```


```bash
$ docker run -d --name south-system -v ~/data:/opt/app/data registry.gitlab.com/andresilvaalves/south-system
```