package io.andresilvaalves.southsystem.processor;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.*;

@Component
public class DirectoryWatcher {

    @Value("${southsystem.data.in}")
    private String inputPath;

    @Value("${southsystem.data.out}")
    private String outputPath;

    public void watch() throws Exception {

        Path path = Paths.get(inputPath);
        WatchService watchService = FileSystems.getDefault().newWatchService();
        path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
        WatchKey key;

        while ((key = watchService.take()) != null) {
            for (WatchEvent<?> event : key.pollEvents()) {

                String filename = String.format("%s",event.context());
                String pathname = StringUtils.joinWith("/", inputPath, filename);
                File file = new File(pathname);

                FileProcessor fileProcessor = new FileProcessor(file, outputPath);
                fileProcessor.start();
            }
            key.reset();
        }
    }
}
