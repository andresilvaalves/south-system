package io.andresilvaalves.southsystem.commons;

public interface FileReader<T> {

    FileDTO reader(final T line);
}
