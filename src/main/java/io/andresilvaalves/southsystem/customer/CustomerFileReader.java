package io.andresilvaalves.southsystem.customer;

import io.andresilvaalves.southsystem.commons.FileDTO;
import io.andresilvaalves.southsystem.commons.FileReader;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("customer")
public class CustomerFileReader implements FileReader<String[]> {

    public static final int FIELD_CNPJ = 1;
    public static final int FIELD_NAME = 2;
    public static final int FIELD_BUSINESS_AREA = 3;

    @Override
    public FileDTO reader(final String[] line) {

        return CustomerDTO.builder()
                          .cnpj(line[FIELD_CNPJ])
                          .name(line[FIELD_NAME])
                          .bussinesArea(line[FIELD_BUSINESS_AREA])
                          .build();

    }
}
